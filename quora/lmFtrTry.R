packages <- c("readr", "dplyr", "purrr", "stringr", "xgboost", "lubridate")
purrr::walk(packages, library, character.only = TRUE, warn.conflicts = FALSE)
library("stringdist") 
library("syuzhet")
library("tm")

## data

train = read.csv("train.csv")
 
#rnd = sample(nrow(train), 0.4*nrow(train) )
#data = train[rnd,]

# lwn dist
data$lwn_dist = stringdist(data$question1 , data$question2  )

# The lcs (longest common substring) distance returns the number of 
# characters that are not part of the lcs.
data$lcs = stringdist(data$question1 , data$question2, method="lcs"  )

data$lwn_dist = stringdist(data$question1 , data$question2, method="lv"  )  

# q-grams are based on the difference between occurrences of q consecutive characters
data$qGrams1 = stringdist(data$question1 , data$question2, method="qgram", q=1  )
data$qGrams2 = stringdist(data$question1 , data$question2, method="qgram", q=2  )

 data$jac  = stringdist(data$question1 , data$question2, method="jaccard" )
 # df$stp1 <-   sapply(df$question1, function(x){      sum(  strsplit(x, " ")[[1]]  %in% stopwords("en")) / length( strsplit(x, " ")[[1]])    }  )  
 # sum(  strsplit("x in a box", " ")[[1]]  %in% stopwords("en")) / length( strsplit("x in a box", " ")[[1]] )
 
# Jaro-distance:
data$Jaro = stringdist(data$question1 , data$question2, method="jw" )
#  Jaro-Winkler
data$Jaro_Winkler = stringdist(data$question1 , data$question2, method="jw", p=0.1 )
#data$Jaro_Winkler = stringdist(data$question1 , data$question2, method="soundex")

data2 = data[,c(7:13,6)]
lmDifSnt = lm( as.numeric(is_duplicate)   ~  lwn_dist + Jaro_Winkler +Jaro+lcs +qGrams1+qGrams2, data = data2)
summary(lmDifSnt)


##############


# LogLossBinary = function(actual, predicted, eps = 1e-15) {
#   predicted = pmin(pmax(predicted, eps), 1-eps)
#   - (sum(actual * log(predicted) + (1 - actual) * log(1 - predicted))) / length(actual)
# }

# drc_tst = c(rep(1,21370),rep(0,1999580))
# mdl_drc  = c(rep(0,21370),rep(0,1999580))
# LogLossBinary(drc_tst, mdl_drc)
# # 
#  tbl = table(mdl_drc, drc_tst)
#  sum(diag(tbl)) / sum( tbl )










